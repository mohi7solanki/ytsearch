import os
from datetime import datetime

from celery import shared_task
from django.contrib.postgres.search import SearchVector

from core.client import YouTubeAPIClient
from core.models import Video


@shared_task
def store_latest_videos():
    """Task to fetch latest videos of a search query and store in DB"""

    query = os.getenv('VIDEO_SEARCH_QUERY')
    keys = os.getenv('API_KEYS').split()
    client = YouTubeAPIClient(api_keys=keys)

    # Get the latest video based on publish time, so we can use this
    # timestamp as an anchor to fetch latest videos after this timestamp
    last_video = (
        Video.objects.values_list('publish_time', named=True)
        .order_by('-publish_time')
        .first()
    )
    published_after = last_video.publish_time.isoformat() if last_video else None
    videos = []
    for video_info in client.fetch_videos(query=query, published_after=published_after):
        videos.append(
            Video(
                slug=video_info['id']['videoId'],
                title=video_info['snippet']['title'],
                description=video_info['snippet']['description'],
                thumbnail_url=video_info['snippet']['thumbnails']['default']['url'],
                publish_time=datetime.fromisoformat(
                    video_info['snippet']['publishedAt'].replace('Z', '+00:00'),
                ),
            )
        )
    Video.objects.bulk_create(videos, batch_size=100)

    # Update the search vectors
    Video.objects.update(search_vector=SearchVector('title', 'description'))