from typing import Generator, List

import requests


class YouTubeAPIClient:
    """Client to interact with YouTube API"""

    class APIQuotaExhausted(Exception):
        """Number of API calls has exceeded the given quota"""

    BASE_URL = "https://youtube.googleapis.com/youtube/v3"

    def __init__(self, api_keys: List[str]) -> None:
        self.api_keys = api_keys
    
    def _request(
        self, method: str, url: str, keys: Generator = None, **kwargs: dict
    ) -> dict:
        """Helper method to make actual API calls to YouTube"""
        
        if keys is None:
            keys = iter(self.api_keys)

        params = kwargs.get('params', {})
    
        try:
            # Use the next key from the `keys` generator
            params['key'] = next(keys)
        except StopIteration:
            # At this point, we have used all available API keys,
            # raise APIQuotaExhausted 
            raise self.APIQuotaExhausted
    
        resp = requests.request(method=method, url=url, **kwargs)
        if resp.status_code == 403:
            # we have exhausted our quota of API calls
            # try again with new API key if available.
            return self._request(method=method, url=url, keys=keys, **kwargs)
        resp.raise_for_status()
        return resp.json()

    def fetch_videos(self, query: str, published_after: str = None) -> dict:
        """Fetch list of videos for the given query since the given datetime"""
        query_params = {
            "part": "snippet",
            "type": "video",
            "order": "date",
            "maxResults": 50,
            "q": query,
        }
        if published_after is not None:
            query_params['publishedAfter'] = published_after
        data = []
        while True:
            response = self._request(
                method="GET", url=f"{self.BASE_URL}/search", params=query_params
            )
            data.extend(response["items"])
            # handle pagination
            if "nextPageToken" not in response:
                break
            query_params["pageToken"] = response["nextPageToken"]
        return data
