from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVectorField
from django.db import models



class Video(models.Model):
    """Store YouTube video data"""

    slug = models.SlugField()
    title = models.CharField(max_length=256)
    description = models.TextField()
    publish_time = models.DateTimeField()
    thumbnail_url = models.URLField()
    search_vector = SearchVectorField(null=True)

    class Meta:
        indexes = [
            models.Index(fields=['-publish_time']),
            GinIndex(fields=['search_vector']),
        ]

    def __str__(self):
        return self.slug