from django.contrib.postgres.search import SearchQuery
from rest_framework import generics

from core.models import Video
from core.serializers import VideoSerializer


class VideoListAPIView(generics.ListAPIView):
    """API view of paginated video list"""

    serializer_class = VideoSerializer
    model = Video
    paginate_by = 100

    def get_queryset(self):
        q = self.request.query_params.get("q")
        if q is None:
            return self.model.objects.order_by('-publish_time')
        return self.model.objects.filter(search_vector=SearchQuery(q))
