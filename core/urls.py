from django.urls import path

from core.views import VideoListAPIView


urlpatterns = [
    path('videos/', VideoListAPIView.as_view(), name="video_list")
]
