from django.contrib import admin
from django.contrib.auth.models import Group, User

from core.models import Video

admin.site.site_header = 'YouTube Search'
admin.site.site_title = 'YouTube Search'

admin.site.unregister(Group)
admin.site.unregister(User)


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    pass
