# YTSearch
An application to fetch YouTube videos details periodically for a given query and store it in a DB.


## API

```
GET /api/videos/
```

Returns the list of video data sorted in descending order of published datetime.

Optionally, You can search for videos using their title and description by passing `q` query parameter.

```
GET /api/videos/?q=tea how
```

## How to setup and run locally

Requirements - Docker and Docker Compose.


```
1. Clone the repository and cd into it
2. Rename file env.sample to .env
3. Add your YouTube API keys in .env file with name `API_KEYS` (If you want to provide more than one key then add them separated by space e.g API_KEYS=key1 key2)
4. `Run docker compose up` 🚀
````

