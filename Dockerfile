FROM python:3.9.6-slim-buster

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1 \
    POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT=true \
    POETRY_VIRTUALENVS_CREATE=false

RUN apt-get update && apt-get install -qq -y build-essential curl netcat libpq-dev

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python

ENV PATH="$POETRY_HOME/bin:$PATH"

RUN mkdir /app

WORKDIR /app

COPY ./poetry.lock ./

COPY ./pyproject.toml ./

RUN poetry install --no-dev

COPY . .

ENTRYPOINT ["/app/entrypoint.sh"]
