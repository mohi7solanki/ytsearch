serveapp:
	python manage.py runserver 0.0.0.0:8000

servecelery:
	celery -A ytsearch worker -l info -B

migrate:
	python manage.py migrate --noinput -v 3
